package com.wy.td;


import com.wy.td.entity.TasCarTrack;
import com.wy.td.entity.VehicleEntity;
import com.wy.td.param.TasCarParams;
import com.wy.td.server.TaosStateServer;
import com.wy.td.server.TaosTrackServer;
import com.wy.td.utils.DateUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j2
public class DecodeToolTest {

    @Test
    public void testTDengine() {
        VehicleEntity entity = new VehicleEntity();
        entity.setMessageTime(DateUtil.getsTime());
        entity.setVehicleNoNumber("LVZZ5TRBXGA597729");
        entity.setVehicleTerminalCode("117512300004731");
        entity.setAzimuth("300");
        entity.setLatitude("29.509912455746367");
        entity.setLongitude("106.44053283622209");
        entity.setLatitudeTab("N");
        entity.setLongitudeTab("E");
        entity.setOnlineStatus("1");
        entity.setOriginalLatitude("29.509912455746367");
        entity.setOriginalLongitude("106.44053283622209");
        entity.setSpeed("999");
        new TaosTrackServer().insertTrack(entity);
        entity.setCarState("1");
        entity.setBonnetStatus("1");
        entity.setLeftFrontStatus("5");
        entity.setLeftRearStatus("2");
        entity.setRightFrontStatus("0");
        entity.setRightRearStatus("1");
        entity.setTrunkStatus("0");
        entity.setHeadlampsStatus("6");
        entity.setLightLlampStatus("1");
        entity.setLeftFrontWindowStatus("0");
        entity.setLeftRearWindowStatus("1");
        entity.setRightFrontWindowStatus("12");
        entity.setRightRearWindowStatus("1");
        entity.setSkyLight("5");
        entity.setLeftFrontLockStatus("5");
        entity.setLeftRearLockStatus("1");
        entity.setRightFrontLockStatus("2");
        entity.setRightRearLockStatus("3");
        entity.setBatteryConnectStatus("1");
        entity.setObdSpeed("54.1");
        entity.setVehicleVoltage("36");
        entity.setCentralLockStatus("1");
        entity.setChargingState("0");
        entity.setDoorStatus("0");
        entity.setDumpEndurance("254");
        entity.setDumpEnergy("58");
        entity.setFuelLineStatus("2");
        entity.setGears("1");
        entity.setLightStatus("1");
        entity.setOriginalGears("N");
        entity.setOriginalTotalDistance("25410");
        entity.setTotalDistance("35214");
        entity.setWindowStatus("1");
        entity.setBrakeStatus("2");
        entity.setKeyStatus("1");
        entity.setOilQuantity("45");
        new TaosStateServer().insertState(entity);
    }

    @Test
    public void testFindTDengine() {
        TasCarParams params = new TasCarParams();
        params.setVehicleNoNumber("LVZZ5TRBXGA597729");
        params.setVehicleTerminalCode("117512300004731");
        TasCarTrack tasCarTrack = new TaosTrackServer().findTasCarTrack(params);
        log.info("查询得到车辆最新轨迹信息：{}", tasCarTrack);
    }

}
