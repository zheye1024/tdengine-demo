package com.wy.td.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author zheye
 */
@Configuration
public class ConstantYmlUtil {

    public static String propertyJdbcUrl;

    public static String propertyKeyUser;

    public static String propertyKeyPassword;

    public static String propertyKeyConfigDir;


    @Value("${xiaozu.tdengine.url}")
    public void setPropertyJdbcUrl(String propertyJdbcUrl) {
        ConstantYmlUtil.propertyJdbcUrl = propertyJdbcUrl;
    }

    @Value("${xiaozu.tdengine.user}")
    public void setPropertyKeyUser(String propertyKeyUser) {
        ConstantYmlUtil.propertyKeyUser = propertyKeyUser;
    }

    @Value("${xiaozu.tdengine.password}")
    public void setPropertyKeyPassword(String propertyKeyPassword) {
        ConstantYmlUtil.propertyKeyPassword = propertyKeyPassword;
    }

    @Value("${xiaozu.tdengine.config_dir}")
    public void setPropertyKeyConfigDir(String propertyKeyConfigDir) {
        ConstantYmlUtil.propertyKeyConfigDir = propertyKeyConfigDir;
    }
}
