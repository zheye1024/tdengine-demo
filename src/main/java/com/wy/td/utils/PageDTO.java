package com.wy.td.utils;

import lombok.Data;

import java.util.List;

/**
 * @Author: 80906
 * @Date: 2022/5/26 21:25
 */
@Data
public class PageDTO<T> {
    private List<T> records;
    private Integer total = 0;
    private Integer size = 20;
    private Integer current = 1;
    private Integer totalPage = 0;

    public PageDTO() {
    }

    public PageDTO(Long current, Long size) {
        if (current != null && current != 0) {
            this.size = size.intValue();
        }
        if (size != null && size != 0) {
            this.current = current.intValue();
        }
    }

    /**
     * 分页
     *
     * @param list     列表数据
     * @param total    总记录数
     * @param pageSize 每页记录数
     * @param pageNo   当前页数
     */
    public PageDTO(List<T> list, Long total, Long pageSize, Long pageNo) {
        this.records = list;
        this.total = (total != null ? total.intValue() : 0);
        this.size = (pageSize != null ? pageSize.intValue() : 0);
        this.current = (pageNo != null ? pageNo.intValue() : 0);
        if (pageSize != null && total != null) {
            this.totalPage = (int) Math.ceil((double) total / pageSize);
        }
    }


    public void setTotal(int total) {
        this.total = total;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public void setTotal(Long total) {
        if (total != null) {
            this.total = total.intValue();
        }
    }

    public void setSize(Long size) {
        if (size != null) {
            this.size = size.intValue();
        }
    }

    public void setCurrent(Long current) {
        if (current != null) {
            this.current = current.intValue();
        }
    }

    public void setTotalPage(Long totalPage) {
        if (totalPage != null) {
            this.totalPage = totalPage.intValue();
        }
    }
}
