package com.wy.td.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 说明：日期处理 创建人：YZT 修改时间：2015年11月24日
 */
public class DateUtil {
    private static Logger logger = LoggerFactory.getLogger(DateUtil.class);

    /**
     * yyyy
     */
    public final static SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
    /**
     * dd
     */
    public final static SimpleDateFormat sdfDD = new SimpleDateFormat("dd");
    /**
     * yyyy-MM-dd
     */
    public final static SimpleDateFormat sdfDay = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * yyyy-MM
     */
    public final static SimpleDateFormat sdfYearMonth = new SimpleDateFormat("yyyy-MM");
    /**
     * yyyy-MM-dd
     */
    public final static SimpleDateFormat yearsDay = new SimpleDateFormat("yyyyMM");
    /**
     * yyyyMMdd
     */
    public final static SimpleDateFormat sdfDays = new SimpleDateFormat("yyyyMMdd");
    /**
     * yyyy-MM-dd HH:mm:ss
     */
    public final static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * yyyy-MM-dd HH:mm:ss
     */
    public final static SimpleDateFormat sdfsTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
    /**
     * yyyyMMddHHmmss
     */
    public final static SimpleDateFormat sdfTimes = new SimpleDateFormat("yyyyMMddHHmmss");

    /**
     * 获取YYYY格式
     *
     * @return
     */
    public static String getSdfTimes() {
        return sdfTimes.format(new Date());
    }

    /**
     * 获取YYYY格式
     *
     * @return
     */
    public static String getYear() {
        return sdfYear.format(new Date());
    }

    /**
     * 获取YYYYMM格式
     *
     * @return
     */
    public static String getYearsDay() {
        return yearsDay.format(new Date());
    }

    /**
     * 获取YYYY格式
     *
     * @return
     */
    public static String getYear(Date date) {
        return sdfYear.format(date);
    }

    /**
     * 获取YYYY-MM-DD格式
     *
     * @return
     */
    public static String getDay() {
        return sdfDay.format(new Date());
    }

    /**
     * 获取YYYY-MM格式
     *
     * @return
     */
    public static String getYearMonth() {
        return sdfYearMonth.format(new Date());
    }

    /**
     * 获取YYYYMMDD格式
     *
     * @return
     */
    public static String getDays() {
        return sdfDays.format(new Date());
    }

    /**
     * 获取YYYY-MM-DD HH:mm:ss格式
     *
     * @return
     */
    public static String getTime() {
        return sdfTime.format(new Date());
    }

    public static String getsTime() {
        return sdfsTime.format(new Date());
    }

    /**
     * 获取YYYY-MM-DD HH:mm:ss格式
     *
     * @return
     */
    public static String getTime(Date date) {
        return sdfTime.format(new Date());
    }

    /**
     * @param s
     * @param e
     * @return boolean
     * @throws @author fh
     * @Title: compareDate
     * @Description: TODO(日期比较 ， 如果s > = e 返回true 否则返回false)
     */
    public static boolean compareDate(String s, String e) {
        if (formatDate(s) == null || formatDate(e) == null) {
            return false;
        }
        return formatDate(s).getTime() >= formatDate(e).getTime();
    }

    /**
     * 格式化日期
     *
     * @return
     */
    public static Date formatDate(String date) {
        try {
            return sdfDay.parse(date);
        } catch (ParseException e) {
            logger.error("Exception:{}", e);
            return null;
        }
    }

    /**
     * 格式化日期
     *
     * @return
     */
    public static Date formatDateHms(String date) {
        try {
            return sdfTime.parse(date);
        } catch (ParseException e) {
            logger.error("Exception:{}", e);
            return null;
        }
    }

    /**
     * 格式化日期
     *
     * @return
     */
    public static Date formatDateHmsSSS(String date) {
        try {
            return sdfsTime.parse(date);
        } catch (ParseException e) {
            logger.error("Exception:{}", e);
            return null;
        }
    }

    /**
     * 校验日期是否合法
     *
     * @return
     */
    public static boolean isValidDate(String s) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            fmt.parse(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 时间相减得到年数
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static int getDiffYear(String startTime, String endTime) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            int years = (int) (((fmt.parse(endTime).getTime() - fmt.parse(startTime).getTime()) / (1000 * 60 * 60 * 24))
                    / 365);
            return years;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * <li>功能描述：时间相减得到天数
     *
     * @param beginDateStr
     * @param endDateStr
     * @return long
     * @author Administrator
     */
    public static long getDaySub(String beginDateStr, String endDateStr) {
        long day = 0;
        Date beginDate = null;
        Date endDate = null;

        try {
            beginDate = sdfDay.parse(beginDateStr);
            endDate = sdfDay.parse(endDateStr);
        } catch (ParseException e) {
            logger.error("Exception:{}", e);
        }
        day = (endDate.getTime() - beginDate.getTime()) / (24 * 60 * 60 * 1000);
        return day;
    }

    /**
     * 得到n天之后的日期
     *
     * @param days
     * @return
     */
    public static String getAfterDayDate(String days) {
        int daysInt = Integer.parseInt(days);
        // java.util包
        Calendar canlendar = Calendar.getInstance();
        // 日期减 如果不够减会将月变动
        canlendar.add(Calendar.DATE, daysInt);
        Date date = canlendar.getTime();

        SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = sdfd.format(date);

        return dateStr;
    }

    /**
     * 得到n天之后是周几
     *
     * @param days
     * @return
     */
    public static String getAfterDayWeek(String days) {
        int daysInt = Integer.parseInt(days);
        // java.util包
        Calendar calendar = Calendar.getInstance();
        // 日期减 如果不够减会将月变动
        calendar.add(Calendar.DATE, daysInt);
        Date date = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("E");
        String dateStr = sdf.format(date);
        return dateStr;
    }

    /**
     * 获取当前时间前后某月第一天，例如前一个月输入参数为-1
     *
     * @param
     * @return
     */
    public static String monthFirstDay(int num) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        calendar.add(Calendar.MONTH, num);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        String fristDay = sdf.format(calendar.getTime());
        return fristDay;
    }

    /**
     * 获取当前时间前后某月最后一天，例如前一个月输入参数为-1
     *
     * @param
     * @return
     */
    public static String monthLastDay(int num) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        calendar.add(Calendar.MONTH, num);
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        // 设置日历中月份的最大天数
        calendar.set(Calendar.DAY_OF_MONTH, maxDay);
        String lastDay = sdf.format(calendar.getTime());
        return lastDay;
    }

    /**
     * 获取指定时间的下一个月
     *
     * @param date
     * @return
     */
    public static Date nextMonth(Date date, int num) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, num);
        return calendar.getTime();
    }

    /**
     * 获取指定时间的前一天
     *
     * @param date
     * @param num
     * @return
     */
    public static Date beforeDay(Date date, int num) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, num);
        return calendar.getTime();
    }

    /**
     * 获取指定某月某天
     *
     * @param date
     * @param day
     * @return
     */
    public static Date designateDay(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int AllDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        if (day > AllDay) {
            calendar.set(Calendar.DAY_OF_MONTH, AllDay);
        } else {
            calendar.set(Calendar.DAY_OF_MONTH, day);
        }
        calendar.add(Calendar.MONTH, 0);
        return calendar.getTime();
    }

    /**
     * * 取得当月天数
     */
    public static int getCurrentMonthLastDay() {
        Calendar a = Calendar.getInstance();
        // 把日期设置为当月第一天
        a.set(Calendar.DATE, 1);
        // 日期回滚一天,也就是最后一天
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    /**
     * 得到指定月的天数
     *
     * @param year
     * @param month
     * @return
     */
    public static int getMonthLastDay(int year, int month) {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        // 把日期设置为当月第一天
        a.set(Calendar.DATE, 1);
        // 日期回滚一天,也就是最后一天
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    /**
     * 开始时间减去结束时间相隔几个月零几天？
     *
     * @param start 开始日期
     * @param end   结束日期
     *              <p>
     *              LocalDate a = LocalDate.of(2012, 6, 30); LocalDate b =
     *              LocalDate.of(2012, 7, 1); a.isBefore(b) == true a.isBefore(a)
     *              == false b.isBefore(a) == false
     * @return months相差月份，days相差天数,
     */
    public static Map<String, Integer> getDiff(LocalDate start, LocalDate end) {
        Map<String, Integer> map = new HashMap<>();
        /*
         * if (!start.isBefore(end)) { return null; }
         */
        Period period = Period.between(start, end);

        int years = period.getYears();
        int months = period.getMonths();
        int days = period.getDays();
        map.put("years", years);
        map.put("months", months);
        map.put("days", days);
        return map;
    }

    /**
     * 得到指定日期后N天的日期
     *
     * @param inputDate 指定日期
     * @param number    指定日期后的N天，负数即为往前
     * @return
     */
    public static String getAfterMonth(String inputDate, int number) {
        Calendar c = Calendar.getInstance();// 获得一个日历的实例
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(inputDate);// 初始日期
        } catch (Exception e) {

        }
        c.setTime(date);// 设置日历时间
        c.add(Calendar.DATE, number);// 在日历的月份上增加n天
        String strDate = sdf.format(c.getTime());// 得到你想要的日期
        return strDate;
    }

    /**
     * 取得当前日期所在周的最后一天
     *
     * @param date
     * @return
     */
    public static String getLastDayOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek() + 6); // Saturday
        return sdfDay.format(beforeDay(calendar.getTime(), 1));
    }

    /**
     * 解散账期转换
     *
     * @param date
     * @return
     */
    public static String transitionPaymentDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int yearStr = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DATE);
        if (day >= 28) {
            month++;
        }
        if (month < 10) {
            return yearStr + "0" + month;
        } else {
            return yearStr + "" + month;
        }
    }

    /**
     * 取得当前日期所在周的第一天
     *
     * @param date
     * @return
     */
    public static String getFirstDayOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek()); // Sunday
        return sdfDay.format(beforeDay(calendar.getTime(), 1));
    }

    /**
     * 比较两个java.util.Date 的日期（年月日）是否相同
     *
     * @param d1
     * @param d2
     * @return
     */
    public static boolean sameDate(Date d1, Date d2) {
        LocalDate localDate1 = ZonedDateTime.ofInstant(d1.toInstant(), ZoneId.systemDefault()).toLocalDate();
        LocalDate localDate2 = ZonedDateTime.ofInstant(d2.toInstant(), ZoneId.systemDefault()).toLocalDate();
        return localDate1.isEqual(localDate2);
    }

    /*
     * 将时间转换为时间戳
     */
    public static String dateToStamp(String s) throws ParseException {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = simpleDateFormat.parse(s);
        long ts = date.getTime();
        res = String.valueOf(ts);
        return res;
    }

    /*
     * 将时间戳转换为时间
     */
    public static String stampToDate(Long s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(s);
        res = simpleDateFormat.format(date);
        return res;
    }

    /**
     * 时间差，时分秒
     *
     * @param start
     * @param end
     * @return
     * @throws Exception
     */
    public static String timeDifference(String start, String end) throws Exception {
        Date parse = sdfTime.parse(start);
        Date date = sdfTime.parse(end);
        long between = date.getTime() - parse.getTime();
        long day = between / (24 * 60 * 60 * 1000);
        long hour = (between / (60 * 60 * 1000) - day * 24);
        long min = ((between / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (between / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        String result = "";
        if (day != 0) {
            result += day + "天" + hour + "时" + min + "分" + s + "秒";
        } else if (hour != 0) {
            result += hour + "时" + min + "分" + s + "秒";
        } else if (min != 0) {
            result += min + "分" + s + "秒";
        } else {
            result += s + "秒";
        }
        return result;
    }

    public static String timeDifferences(String start, String end) throws Exception {
        Date parse = sdfTime.parse(start);
        Date date = sdfTime.parse(end);
        long between = date.getTime() - parse.getTime();
        return String.valueOf(between);
    }

    /**
     * 两个日期想减得到月份
     *
     * @param start
     * @param end
     * @return
     * @throws Exception
     */
    public static int differMonth(String start, String end) throws Exception {
        Calendar bef = Calendar.getInstance();
        Calendar aft = Calendar.getInstance();
        bef.setTime(sdfYearMonth.parse(start));
        aft.setTime(sdfYearMonth.parse(end));
        int result = aft.get(Calendar.MONTH) - bef.get(Calendar.MONTH);
        int month = (aft.get(Calendar.YEAR) - bef.get(Calendar.YEAR)) * 12;
        return result + month;
    }

    /**
     * 增加（减少-）月
     *
     * @param date
     * @param num
     * @return
     * @throws Exception
     */
    public static String subMonth(String date, int num) throws Exception {
        Date dt = sdfDay.parse(date);
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(dt);
        rightNow.add(Calendar.MONTH, num);
        Date dt1 = rightNow.getTime();
        String reStr = sdfDay.format(dt1);
        return reStr;
    }
}
