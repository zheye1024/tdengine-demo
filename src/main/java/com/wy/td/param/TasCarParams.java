package com.wy.td.param;

import lombok.Data;

import java.util.Date;

/**
 * @Author: 80906
 * @Date: 2022/5/20 16:29
 */
@Data
public class TasCarParams {

    /**
     * 开始时间 - 主键
     */
    private Date startDate;

    /**
     * 结束时间 - 主键
     */
    private Date endDate;

    /**
     * 车机编号 - tags
     */
    private String vehicleTerminalCode;

    /**
     * 车架号 - tags
     */
    private String vehicleNoNumber;

    /**
     * 是否分页查询
     */
    private Boolean pageFlag = Boolean.FALSE;

    /**
     * 起始数量
     */
    private Integer currentNumber = 0;

    /**
     * 数量大小
     */
    private Integer pageSize = 1;

}
