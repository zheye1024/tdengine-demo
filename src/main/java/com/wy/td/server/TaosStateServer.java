package com.wy.td.server;


import com.wy.td.client.TaosClient;
import com.wy.td.entity.TasCarState;
import com.wy.td.entity.TasCarTags;
import com.wy.td.entity.VehicleEntity;
import com.wy.td.param.TasCarParams;
import com.wy.td.utils.DateUtil;
import com.wy.td.utils.PageDTO;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.log4j.Log4j2;

import java.util.List;

/**
 * TDengine 车辆转态操作定义
 *
 * @Author: 80906
 * @Date: 2022/5/19 22:17
 */
@Log4j2
public class TaosStateServer {

    /**
     * 超级表名
     */
    private static final String TAS_CAR_STATE = "tas_car_state";
    /**
     * 数据表前缀
     */
    private static final String TABEL_PRE = "state_";

    /**
     * 插入车辆状态信息
     *
     * @param entity
     */
    public void insertState(VehicleEntity entity) {
        TaosClient taosClient = null;
        try {
            taosClient = new TaosClient(Boolean.TRUE);
            TasCarState tasCarTrack = new TasCarState();
            TasCarTags tags = new TasCarTags();
            this.convert(entity, tasCarTrack, tags);
            taosClient.insertWithStable(TABEL_PRE + entity.getVehicleNoNumber(), TAS_CAR_STATE, tasCarTrack, tags);
        } catch (Exception e) {
            log.error("====插入车辆状态信息异常：{}", e);
        } finally {
            if (taosClient != null) {
                try {
                    taosClient.close();
                } catch (Exception e) {
                    log.error("关闭TDengine 链接异常：{}", e);
                }
            }
        }
    }

    /**
     * 查询得到车辆状态信息-最新状态信息
     *
     * @return
     * @Param params
     */
    public TasCarState findTasCarState(TasCarParams params) {
        TaosClient taosClient = null;
        try {
            if (StringUtils.isBlank(params.getVehicleTerminalCode()) && StringUtils.isBlank(params.getVehicleNoNumber())) {
                log.error("查询指定车辆信息车机编号或车架号同时为空，查询失败！");
                return null;
            }
            taosClient = new TaosClient(Boolean.TRUE);
            // 时间倒叙查询第一条即为车机的最新状态信息
            StringBuilder sql = new StringBuilder("select * from ").append(TAS_CAR_STATE);
            sql.append(this.assembleWhereSql(params));
            sql.append(" order by message_time desc limit 0,1");
            log.info("执行查询sql语句：{}", sql.toString());
            return taosClient.getOne(sql.toString(), TasCarState.class);
        } catch (Exception e) {
            log.error("查询得到车辆状态信息异常：{}", e);
        } finally {
            if (taosClient != null) {
                try {
                    taosClient.close();
                } catch (Exception e) {
                    log.error("关闭TDengine 链接异常：{}", e);
                }
            }
        }
        return null;
    }

    /**
     * 查询得到车辆状态信息-时间段集合
     *
     * @return
     * @Param params
     */
    public List<TasCarState> queryTasCarState(TasCarParams params) {
        TaosClient taosClient = null;
        try {
            taosClient = new TaosClient(Boolean.TRUE);
            StringBuilder sql = new StringBuilder("select * from ").append(TAS_CAR_STATE);
            sql.append(this.assembleWhereSql(params));
            log.info("执行查询集合sql语句：{}", sql.toString());
            return taosClient.getList(sql.toString(), TasCarState.class);
        } catch (Exception e) {
            log.error("查询得到车辆状态信息异常：{}", e);
        } finally {
            if (taosClient != null) {
                try {
                    taosClient.close();
                } catch (Exception e) {
                    log.error("关闭TDengine 链接异常：{}", e);
                }
            }
        }
        return null;
    }

    /**
     * @param params
     * @return
     */
    public PageDTO<TasCarState> queryPageTasCarState(TasCarParams params) {
        TaosClient taosClient = null;
        PageDTO<TasCarState> pageDTO = new PageDTO<>();
        try {
            taosClient = new TaosClient(Boolean.TRUE);
            StringBuilder sql = new StringBuilder("select * from ").append(TAS_CAR_STATE);
            sql.append(this.assembleWhereSql(params));
            if (params.getPageFlag()) {
                sql.append(" limit ").append(params.getCurrentNumber()).append(",").append(params.getPageSize());
            }
            log.info("执行查询集合sql语句：{}", sql.toString());
            List<TasCarState> carStateList = taosClient.getList(sql.toString(), TasCarState.class);
            if (params.getPageFlag()) {
                StringBuilder countSql = new StringBuilder("select count(1) from ").append(TAS_CAR_STATE);
                countSql.append(this.assembleWhereSql(params));
                log.info("执行查询集合sql语句：{}", countSql.toString());
                Integer count = taosClient.getOne(countSql.toString(), Integer.class);
                pageDTO.setTotal(count);
            }
            pageDTO.setRecords(carStateList);
        } catch (Exception e) {
            log.error("查询得到车辆状态信息异常：{}", e);
        } finally {
            if (taosClient != null) {
                try {
                    taosClient.close();
                } catch (Exception e) {
                    log.error("关闭TDengine 链接异常：{}", e);
                }
            }
        }
        return pageDTO;
    }

    /**
     * 查询参数sql定义
     *
     * @param params
     * @return
     */
    private String assembleWhereSql(TasCarParams params) {
        StringBuilder sql = new StringBuilder(" where ");
        boolean flag = Boolean.FALSE;
        if (StringUtils.isNotBlank(params.getVehicleNoNumber())) {
            sql.append(" vehicle_no_number = '").append(params.getVehicleNoNumber()).append("'");
            flag = Boolean.TRUE;
        }
        if (StringUtils.isNotBlank(params.getVehicleTerminalCode())) {
            if (flag) {
                sql.append(" and ");
            }
            sql.append(" vehicle_terminal_code = '").append(params.getVehicleTerminalCode()).append("'");
        }
        if (params.getStartDate() != null) {
            if (flag) {
                sql.append(" and ");
            }
            sql.append(" message_time >= ").append(params.getStartDate().getTime());
            flag = Boolean.TRUE;
        }
        if (params.getEndDate() != null) {
            if (flag) {
                sql.append(" and ");
            }
            sql.append(" message_time <= ").append(params.getEndDate().getTime());
            flag = Boolean.TRUE;
        }
        return sql.toString();
    }

    private void convert(VehicleEntity entity, TasCarState tasCarState, TasCarTags tags) {
        tags.setVehicleTerminalCode(entity.getVehicleTerminalCode());
        tags.setVehicleNoNumber(entity.getVehicleNoNumber());

        tasCarState.setMessageTime(DateUtil.formatDateHmsSSS(entity.getMessageTime()));
        tasCarState.setCarState(entity.getCarState());
        tasCarState.setBonnetStatus(entity.getBonnetStatus());
        tasCarState.setLeftFrontStatus(entity.getLeftFrontStatus());
        tasCarState.setLeftRearStatus(entity.getLeftRearStatus());
        tasCarState.setRightFrontStatus(entity.getRightFrontStatus());
        tasCarState.setRightRearStatus(entity.getRightRearStatus());
        tasCarState.setTrunkStatus(entity.getTrunkStatus());
        tasCarState.setHeadLampStatus(entity.getHeadlampsStatus());
        tasCarState.setLightLampStatus(entity.getLightLlampStatus());
        tasCarState.setLeftFrontWindowStatus(entity.getLeftFrontWindowStatus());
        tasCarState.setLeftRearWindowStatus(entity.getLeftRearWindowStatus());
        tasCarState.setRightFrontLockStatus(entity.getRightFrontLockStatus());
        tasCarState.setRightRearLockStatus(entity.getRightRearLockStatus());
        tasCarState.setRightFrontWindowStatus(entity.getRightFrontWindowStatus());
        tasCarState.setRightRearWindowStatus(entity.getRightRearWindowStatus());
        tasCarState.setSkyLight(entity.getSkyLight());
        tasCarState.setLeftFrontLockStatus(entity.getLeftFrontLockStatus());
        tasCarState.setLeftRearLockStatus(entity.getLeftRearLockStatus());
        tasCarState.setBatteryConnectStatus(entity.getBatteryConnectStatus());
        tasCarState.setObdSpeed(entity.getObdSpeed());
        tasCarState.setVehicleVoltage(entity.getVehicleVoltage());
        tasCarState.setCentralLockStatus(entity.getCentralLockStatus());
        tasCarState.setChargingState(entity.getChargingState());
        tasCarState.setDoorStatus(entity.getDoorStatus());
        tasCarState.setDumpEndurance(entity.getDumpEndurance());
        tasCarState.setDumpEnergy(entity.getDumpEnergy());
        tasCarState.setFuelLineStatus(entity.getFuelLineStatus());
        tasCarState.setGears(entity.getGears());
        tasCarState.setLightStatus(entity.getLightStatus());
        tasCarState.setOriginalGears(entity.getOriginalGears());
        tasCarState.setOriginalTotalDistance(entity.getOriginalTotalDistance());
        tasCarState.setTotalDistance(entity.getTotalDistance());
        tasCarState.setWindowStatus(entity.getWindowStatus());
        tasCarState.setBrakeStatus(entity.getBrakeStatus());
        tasCarState.setKeyStatus(entity.getKeyStatus());
        tasCarState.setOilQuantity(entity.getOilQuantity());
    }
}
