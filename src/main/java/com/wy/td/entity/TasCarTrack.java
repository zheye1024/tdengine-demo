package com.wy.td.entity;

import lombok.Data;

import java.util.Date;

/**
 * @Author: 80906
 * @Date: 2022/5/19 16:40
 */

@Data
public class TasCarTrack {

    /**
     * -- 车机轨迹信息超级表
     *CREATE STABLE IF NOT EXISTS tas_car_track(message_time TIMESTAMP,azimuth nchar(32),latitude nchar(32),longitude nchar(32),
     * latitude_tab nchar(32),longitude_tab nchar(32),online_status nchar(4),original_latitude nchar(32),original_longitude nchar(32),speed nchar(32))
     * TAGS(vehicle_terminal_code nchar(32), vehicle_no_number nchar(32));
     */

    /**
     * -- 车机轨迹信息插入用例
     * INSERT INTO LM8A7A18XLA001512 USING tas_car_track TAGS('868860051317905','LM8A7A18XLA001512') VALUES('2022-05-19 10:45:35.999','10','29.628593','106.52747','N','E','1','29.628593','106.52747','21');
     * INSERT INTO LM8A7A18XLA001512 USING tas_car_track TAGS('868860051317905','LM8A7A18XLA001512') VALUES('2022-05-19 10:48:30.123','139','22.67733','114.28685','S','W','0','22.673733','114.28685','0');
     */

    /**
     * messageTime: 写入时间 主键索引
     * vehicleTerminalCode: 车架号 tags
     * vehicleNoNumber: 车机号 tags
     * azimuth： 航向
     * latitude： 纬度
     * longitude： 经度
     * latitudeTab：纬度标记 N (北纬) S (南纬)
     * longitudeTab：经度标记 E（东经）W（西经）
     * onlineStatus： 在线状态
     * originalLatitude： 原始纬度
     * originalLongitude： 原始经度
     * speed：卫星速度
     */
    private Date messageTime;
    private String azimuth;
    private String latitude;
    private String longitude;
    private String latitudeTab;
    private String longitudeTab;
    private String onlineStatus;
    private String originalLatitude;
    private String originalLongitude;
    private String speed;


}
