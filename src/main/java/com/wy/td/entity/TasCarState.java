package com.wy.td.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @Author: 80906
 * @Date: 2022/5/19 16:41
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TasCarState extends TasCarTags {

    /**
     *-- 车机状态信息超级表
     * CREATE STABLE IF NOT EXISTS tas_car_state(message_time TIMESTAMP,car_state nchar(8),bonnet_status nchar(8),left_front_status nchar(8),left_rear_Status nchar(8),right_front_status nchar(8),
     * right_rear_status nchar(8),trunk_status nchar(8),head_lamp_status nchar(8),light_lamp_status nchar(8),left_front_window_status nchar(8),left_rear_window_status nchar(8),right_front_window_status nchar(8),
     * right_rear_window_status nchar(8),sky_light nchar(8),left_front_lock_status nchar(8),left_rear_lock_status nchar(8),right_front_lock_status nchar(8),right_rear_lock_status nchar(8),battery_connect_status nchar(8),
     * obd_speed nchar(32),vehicle_voltage nchar(32),central_lock_status nchar(8),charging_state nchar(8),door_status nchar(8),dump_endurance nchar(32),dump_energy nchar(8),fuel_line_status nchar(8),gears nchar(8),
     * light_status nchar(8),original_gears nchar(8),original_total_distance nchar(32),total_distance nchar(32),window_status nchar(8),brake_status nchar(8),key_status nchar(8),oil_quantity nchar(8))
     *  TAGS(vehicle_terminal_code nchar(32), vehicle_no_number nchar(32));
     */

    /**
     * messageTime: 写入时间 主键索引
     * vehicleTerminalCode: 车架号 tags
     * vehicleNoNumber: 车机号 tags
     * carState: 车辆状态
     * bonnetStatus：前盖
     * leftFrontStatus：左前门
     * leftRearStatus：左后门
     * rightFrontStatus：右前门
     * rightRearStatus：右后门
     * trunkStatus：后备箱
     * headLampStatus：前照灯
     * lightLampStatus：转向灯
     * leftFrontWindowStatus：左前窗
     * leftRearWindowStatus：左后窗
     * rightFrontWindowStatus：右前窗
     * rightRearWindowStatus：右后窗
     * skyLight：天窗
     * leftFrontLockStatus：左前门锁
     * leftRearLockStatus：左后门锁
     * rightFrontLockStatus：右前门锁
     * rightRearLockStatus：右后门锁
     * batteryConnectStatus：电瓶链接状态
     * obdSpeed：车机速度
     * vehicleVoltage：电压
     * centralLockStatus：解锁状态
     * chargingState：充电状态
     * doorStatus：车门状态
     * dumpEndurance：续航里程
     * dumpEnergy：电量
     * fuelLineStatus：油路状态
     * gears：原始档位
     * lightStatus：车灯状态
     * originalGears：原始档位
     * originalTotalDistance：原始总里程
     * totalDistance：总里程
     * windowStatus：车窗状态
     * brakeStatus: 手刹状态，1-拉起，2-放下
     * keyStatus：车钥匙状态：1-插入，2-拔出，0-无效，空-不支持该状态
     * oilQuantity: 油量
     */
    /**
     * TDengine 值得第一个字段必须是时间戳
     * 设计表时，只设计第一个字段为DaTe,其他字段为字符串格式存储
     */
    private Date messageTime;
    private String carState;
    private String bonnetStatus;
    private String leftFrontStatus;
    private String leftRearStatus;
    private String rightFrontStatus;
    private String rightRearStatus;
    private String trunkStatus;
    private String headLampStatus;
    private String lightLampStatus;
    private String leftFrontWindowStatus;
    private String leftRearWindowStatus;
    private String rightFrontLockStatus;
    private String rightRearLockStatus;
    private String rightFrontWindowStatus;
    private String rightRearWindowStatus;
    private String skyLight;
    private String leftFrontLockStatus;
    private String leftRearLockStatus;
    private String batteryConnectStatus;
    private String obdSpeed;
    private String vehicleVoltage;
    private String centralLockStatus;
    private String chargingState;
    private String doorStatus;
    private String dumpEndurance;
    private String dumpEnergy;
    private String fuelLineStatus;
    private String gears;
    private String lightStatus;
    private String originalGears;
    private String originalTotalDistance;
    private String totalDistance;
    private String windowStatus;
    private String brakeStatus;
    private String keyStatus;
    private String oilQuantity;

}
