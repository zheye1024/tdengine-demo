package com.wy.td.entity;


import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * ps: 车辆轨迹/状态实体类
 *
 * @author:ykbaiming@outlook.com
 * @date:2018/7/12
 */
@Setter
@Getter
public class VehicleEntity {
    /**
     * 0:定位信息 1:状态信息
     */
    private int dataType = 1;
    /**
     * 车辆ID
     */
    private String vehicleId = "0";
    /**
     * 车机编号
     */
    private String vehicleTerminalCode;
    /**
     * 车机牌
     */
    private String carLicense;
    /**
     * 车型
     */
    private String vehicleType;
    /**
     * 电量
     */
    private String dumpEnergy;
    /**
     * 油量
     */
    private String oilQuantity;

    /**
     * 续航能力
     */
    private String dumpEndurance;
    /**
     * 总里程
     */
    private String totalDistance;
    /**
     * 原始总里程
     */
    private String originalTotalDistance;
    /**
     * 车辆状态
     */
    private String carState;
    /**
     * 油路状态
     */
    private String fuelLineStatus;
    /**
     * 电瓶连接状态
     */
    private String batteryConnectStatus;
    /**
     * 电压
     */
    private String vehicleVoltage;
    /**
     * 信号强度
     */
    private String signalStrength;
    /**
     * 卫星速度
     */
    private String speed;
    /**
     * 车机速度
     */
    private String obdSpeed;
    /**
     * 故障码
     */
    private String errorCode;
    /**
     * 报文时间 这里不是时间戳-而是时间字符串格式:2018-07-25 17:23:15
     */
    private String messageTime;
    /**
     * 添加时间 13位时间戳
     */
    private String addTime = System.currentTimeMillis() + "";
    /**
     * 纬度
     */
    private String latitude;
    /**
     * 纬度标记 N (北纬) S (南纬)
     */
    private String latitudeTab;
    /**
     * 经度
     */
    private String longitude;

    /**
     * 原始纬度
     */
    private String originalLatitude;

    /**
     * 原始经度
     */
    private String originalLongitude;

    /**
     * 原始档位
     */
    private String originalGears;

    /**
     * 经度标记 E（东经）W（西经）
     */
    private String longitudeTab;
    /**
     * 航向
     */
    private String azimuth;
    /**
     * 磁偏角_度
     */
    private String declinationDegrees;
    /**
     * 磁偏角方向 E:东 W西
     */
    private String declinationDirection;
    /**
     * 模式指示:A=自主定位，D=差分，E=估算，N=数据无效
     * *后hh为$到*所有字符的异或和
     */
    private String indicated;
    /**
     * 海拔:单位米
     */
    private String elevation;
    /**
     * 预留字段
     */
    private String reserved;
    /**
     * 电压
     */
    private String voltage;
    /**
     * 充电状态
     */
    private String chargingState;
    /**
     * 报警标识
     */
    private String warningMark;
    /**
     * 区域ID
     */
    private String areaId;
    /**
     * 中控锁
     */
    private String centralLockStatus;
    /**
     * 左前门锁
     */
    private String leftFrontLockStatus;
    /**
     * 左后门锁
     */
    private String leftRearLockStatus;
    /**
     * 右前门锁
     */
    private String rightFrontLockStatus;
    /**
     * 右后门锁
     */
    private String rightRearLockStatus;

    /**
     * 手刹状态，1-拉起，2-放下
     */
    private String brakeStatus;

    /**
     * 车钥匙状态：1-插入，2-拔出，0-无效，空-不支持该状态
     */
    private String keyStatus;

    /**
     * 车门
     */
    private String doorStatus;

    /**
     * 前盖
     */
    private String bonnetStatus;

    /**
     * 左前门
     */
    private String leftFrontStatus;
    /**
     * 左后门
     */
    private String leftRearStatus;
    /**
     * 右前门
     */
    private String rightFrontStatus;
    /**
     * 右后门
     */
    private String rightRearStatus;

    /**
     * 后备箱
     */
    private String trunkStatus;
    /**
     * 车灯
     */
    private String lightStatus;
    /**
     * 前照灯
     */
    private String headlampsStatus;
    /**
     * 转向灯
     */
    private String lightLlampStatus;
    /**
     * 车窗
     */
    private String windowStatus;
    /**
     * 左前窗
     */
    private String leftFrontWindowStatus;
    /**
     * 左后窗
     */
    private String leftRearWindowStatus;
    /**
     * 右前窗
     */
    private String rightFrontWindowStatus;
    /**
     * 右后窗
     */
    private String rightRearWindowStatus;
    /**
     * 天窗
     */
    private String skyLight;
    /**
     * 小电瓶电压
     */
    private String batteryVoltage;
    /**
     * 修改时间时间戳
     */
    private long updateTimestamp = 0;

    /**
     * 档位：1-P 2-R 3-N 4-D(S) 5-M
     */
    private String gears;

    /**
     * 在线状态（目前为易微行专属）
     */
    private String onlineStatus;

    /**
     * 事件Code
     */
    private String eventCode;

    /**
     * 车架号
     */
    private String vehicleNoNumber;

    /**
     * 车牌号
     */
    private String plateNumber;

    /**
     * 车辆类型
     */
    private String vehicleUse;

    public VehicleEntity() {
    }

    /**
     * 取两个文本之间的文本值
     *
     * @param text
     * @param left
     * @param right
     * @return
     */
    public static String getSubString(String text, String left, String right) {
        String result = "";
        int zLen;
        if (left == null || left.isEmpty()) {
            zLen = 0;
        } else {
            zLen = text.indexOf(left);
            if (zLen > -1) {
                zLen += left.length();
            } else {
                zLen = 0;
            }
        }
        int yLen = text.indexOf(right, zLen);
        if (yLen < 0 || right == null || right.isEmpty()) {
            yLen = text.length();
        }
        result = text.substring(zLen, yLen);
        return result;
    }

    @Override
    public String toString() {
        return "VehicleEntity{" +
                "dataType=" + dataType +
                ", vehicleId='" + vehicleId + '\'' +
                ", vehicleTerminalCode='" + vehicleTerminalCode + '\'' +
                ", carLicense='" + carLicense + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", dumpEnergy='" + dumpEnergy + '\'' +
                ", oilQuantity='" + oilQuantity + '\'' +
                ", dumpEndurance='" + dumpEndurance + '\'' +
                ", totalDistance='" + totalDistance + '\'' +
                ", originalTotalDistance='" + originalTotalDistance + '\'' +
                ", carState='" + carState + '\'' +
                ", fuelLineStatus='" + fuelLineStatus + '\'' +
                ", vehicleVoltage='" + vehicleVoltage + '\'' +
                ", signalStrength='" + signalStrength + '\'' +
                ", speed='" + speed + '\'' +
                ", obdSpeed='" + obdSpeed + '\'' +
                ", gears='" + gears + '\'' +
                ", originalGears='" + originalGears + '\'' +
                ", centralLockStatus='" + centralLockStatus + '\'' +
                ", brakeStatus='" + brakeStatus + '\'' +
                ", doorStatus='" + doorStatus + '\'' +
                ", lightStatus='" + lightStatus + '\'' +
                ", windowStatus='" + windowStatus + '\'' +
                ", batteryConnectStatus='" + batteryConnectStatus + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", messageTime='" + messageTime + '\'' +
                ", addTime='" + addTime + '\'' +
                ", latitude='" + latitude + '\'' +
                ", originalLatitude='" + originalLatitude + '\'' +
                ", latitudeTab='" + latitudeTab + '\'' +
                ", longitude='" + longitude + '\'' +
                ", originalLongitude='" + originalLongitude + '\'' +
                ", longitudeTab='" + longitudeTab + '\'' +
                ", azimuth='" + azimuth + '\'' +
                ", declinationDegrees='" + declinationDegrees + '\'' +
                ", declinationDirection='" + declinationDirection + '\'' +
                ", indicated='" + indicated + '\'' +
                ", elevation='" + elevation + '\'' +
                ", reserved='" + reserved + '\'' +
                ", voltage='" + voltage + '\'' +
                ", chargingState='" + chargingState + '\'' +
                ", warningMark='" + warningMark + '\'' +
                '}';
    }
}
