package com.wy.td.entity;

import lombok.Data;

/**
 * @Author: 80906
 * @Date: 2022/5/24 14:29
 */
@Data
public class TasCarTags {

    private String vehicleTerminalCode;
    private String vehicleNoNumber;

}
